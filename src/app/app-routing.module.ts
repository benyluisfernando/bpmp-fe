import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { GuardGuard } from "./guard/guard.guard";
import { BackmenulayoutComponent } from "./layout/backmenulayout/backmenulayout.component";
import { FullmenulayoutComponent } from "./layout/fullmenulayout/fullmenulayout.component";
import { MainmenulayoutComponent } from "./layout/mainmenulayout/mainmenulayout.component";
import { NomenulayoutComponent } from "./layout/nomenulayout/nomenulayout.component";
import { ErrorpageComponent } from "./pages/errorpage/errorpage.component";
import { HomeComponent } from "./pages/home/home.component";
import { HomeadminComponent } from "./pages/homeadmin/homeadmin.component";
import { BpmpinterceptComponent } from "./pages/bpmp/bpmphome/bpmpintercept.component";
import { LoginComponent } from "./pages/login/login.component";
import { ForgotpasswordComponent } from "./pages/forgotpassword/forgotpassword.component";
import { ApplicationdetailComponent } from "./pages/root/applicationgroup/applicationdetail/applicationdetail.component";
import { ApplicationgroupComponent } from "./pages/root/applicationgroup/applicationgroup.component";
import { ApplicationsComponent } from "./pages/root/applications/applications.component";
import { EventlogsComponent } from "./pages/root/eventlogs/eventlogs.component";
import { OauthsettingsComponent } from "./pages/root/oauthsettings/oauthsettings.component";
import { ResourceusageComponent } from "./pages/root/resourceusage/resourceusage.component";
import { SmtpaccountsComponent } from "./pages/root/smtpaccounts/smtpaccounts.component";
import { UserdetailComponent } from "./pages/root/users/userdetail/userdetail.component";
import { UsersComponent } from "./pages/root/users/users.component";
import { SystemparamComponent } from "./pages/root/systemparam/systemparam.component";
import { SystemparamdetailComponent } from "./pages/root/systemparam/systemparamdetail/systemparamdetail.component";
import { ProfileComponent } from "./pages/root/profile/profile.component";
import { ResetpasswordComponent } from "./pages/forgotpassword/resetpassword/resetpassword.component";
import { VerifikasiemailComponent } from "./pages/forgotpassword/verifikasiemail/verifikasiemail.component";
import { BpmphomeComponent } from "./pages/bpmp/bpmphome/bpmphome.component";
import { CompanyComponent } from "./pages/bpmp/company/company.component";
import { CompanydetailComponent } from "./pages/bpmp/company/companydetail/companydetail.component";
import { ChanneldetailComponent } from "./pages/bpmp/channel/channeldetail/channeldetail.component";
import { ChannelComponent } from "./pages/bpmp/channel/channel.component";
import { CompanyreqComponent } from "./pages/bpmp/companyreq/companyreq.component";
import { ChannelreqComponent } from "./pages/bpmp/channelreq/channelreq.component";
import { RulesreqComponent } from "./pages/bpmp/rulesreq/rulesreq.component";
import { RulesreqdetailComponent } from "./pages/bpmp/rulesreq/rulesreqdetail/rulesreqdetail.component";
import { RulesComponent } from "./pages/bpmp/rules/rules.component";
import { RulesdetailComponent } from "./pages/bpmp/rules/rulesdetail/rulesdetail.component";
import { TransactionmonitoringComponent } from "./pages/bpmp/transactionmonitoring/transactionmonitoring.component";

const routes: Routes = [
  { path: "", redirectTo: "auth/login", pathMatch: "full" },
  {
    path: ":config",
    canActivate: [GuardGuard],
    component: BpmpinterceptComponent,
  },
  {
    path: ":config",
    canActivate: [GuardGuard],
    component: BpmpinterceptComponent,
  },
  {
    path: "mgm",
    canActivate: [GuardGuard],
    component: MainmenulayoutComponent,
    children: [
      { path: "home", component: BpmphomeComponent },
      { path: "profile", component: ProfileComponent },
      {
        path: "user",
        canActivate: [GuardGuard],
        children: [
          { path: "userslist", component: UsersComponent },
          {
            path: "userslist",
            children: [
              { path: "detail", component: UserdetailComponent },
              { path: "detail#/:id", component: UserdetailComponent },
            ],
          },
        ],
      },
      {
        path: "acl",
        canActivate: [GuardGuard],
        children: [
          { path: "grouplist", component: ApplicationgroupComponent },
          {
            path: "grouplist",
            children: [
              { path: "detail", component: ApplicationdetailComponent },
              { path: "detail#/:id", component: ApplicationdetailComponent },
            ],
          },
        ],
      },
      {
        path: "system",
        canActivate: [GuardGuard],
        children: [
          { path: "sysparam", component: SystemparamComponent },
          {
            path: "sysparam",
            children: [
              { path: "detail", component: SystemparamdetailComponent },
              { path: "detail#/:id", component: SystemparamdetailComponent },
            ],
          },
        ],
      },

      {
        path: "settings",
        canActivate: [GuardGuard],
        children: [
          {
            path: "transactionlist",
            component: TransactionmonitoringComponent,
          },
          { path: "companytlist", component: CompanyComponent },
          {
            path: "companytlist",
            children: [
              { path: "detail", component: CompanydetailComponent },
              { path: "detail#/:id", component: CompanydetailComponent },
            ],
          },

          { path: "channellist", component: ChannelComponent },
          {
            path: "channellist",
            children: [
              { path: "detail", component: ChanneldetailComponent },
              { path: "detail#/:id", component: ChanneldetailComponent },
            ],
          },
          { path: "companyreq", component: CompanyreqComponent },
          {
            path: "companyreq",
            component: CompanyreqComponent,
          },
          
          {
            path: "channelreq",
            component: ChannelreqComponent,
          },

          {
            path: "rules",
            component: RulesComponent,
          },
          {
            path: "rules",
            children: [
              {
                path: "detail",
                component: RulesdetailComponent,
              },
              {
                path: "detail#/:id",
                component: RulesdetailComponent,
              },
            ],
          },
          {
            path: "rulesreq",
            component: RulesreqComponent,
          },
          {
            path: "rulesreq",
            children: [
              {
                path: "detail",
                component: RulesreqdetailComponent,
              },
              {
                path: "detail#/:id",
                component: RulesreqdetailComponent,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    path: "nopage",
    component: BackmenulayoutComponent,
    children: [{ path: "404", component: ErrorpageComponent }],
  },
  {
    path: "auth",
    data: { title: "Login" },
    component: NomenulayoutComponent,
    children: [
      { path: "login", component: LoginComponent },
      { path: "forgotpassword", component: ForgotpasswordComponent },
      { path: "resetpassword/:id", component: ResetpasswordComponent },
      { path: "verifikasiemail/:id", component: VerifikasiemailComponent },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
