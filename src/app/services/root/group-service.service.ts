import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class GroupServiceService {
  reqGroup: any = {};
  groupSetupData: any = {};
  allModules: any[] = [];
  modules: any[] = [];
  menus: any[] = [];
  groupEditData: any = {};
  haveModuleAndMenus: boolean = false;
  constructor(private service: BackendService) { }
  getAllModules(id: any) {
    const url = `adm/accmod/${id}`;
    return this.service.get(url);
  }

  getAllMenuByModuleId(id: any) {
    console.log(">>>>>>>>>>>>>>>>>",id);
    const url = `adm/accmod/menusmodule/${id}`;
    return this.service.get(url);
  }
  getAllMenuByModuleIdEdit(id: any, grpid: any) {
    const url = `adm/accmod/menusmodule/${id}`;
    return this.service.get(url);
  }

  getAllMenuByModuleIdAndGroupId(id: any, groupid: any) {
    const url = `adm/accmod/menusmodule/${id}/${groupid}`;
    return this.service.get(url);
  }

  getModuleWithMenu() {
    const url = 'adm/group/getallmodulewithmenu';
    return this.service.get(url);
  }
  regisGroup(payload: any) {

    console.log(JSON.stringify(payload));



    const url = 'adm/group/regisGroup';
    return this.service.post(url, payload);
  }

  editGroup(payload: any) {
    const url = 'adm/group/editGroup';
    return this.service.post(url, payload);
  }

  editGroupActive(payload: any) {
    const url = 'adm/group/editGroupActive';
    return this.service.post(url, payload);
  }



  getAllGroup() {
    const url = 'adm/group/getAllGroup';
    return this.service.get(url);
  }
  getAllGroupForData() {
    const url = 'adm/group/getAllGroupForData';
    return this.service.get(url);
  }
  getExternalGroup(){
    const url = 'adm/group/getExternalGroup';
    return this.service.get(url);
  }

  getGroupDetail(id: any) {
    const url = `adm/group/getDetailsGroup/${id}`;
    return this.service.get(url);
  }

  deleteGroup(payload: any) {
    const url = 'adm/group/deleteGroup';
    return this.service.post(url, payload);
  }
}
