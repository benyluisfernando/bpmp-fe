import { TestBed } from '@angular/core/testing';

import { SysparamService } from './sysparam.service';

describe('SysparamService', () => {
  let service: SysparamService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SysparamService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
