import { Injectable } from "@angular/core";
import { BackendService } from "../backend.service";

@Injectable({
  providedIn: "root",
})
export class ChannelService {
  constructor(private service: BackendService) {}

  getAllChannel() {
    const url = "api/channel/getChannel";
    return this.service.baseGet(url);
  }

  insertChannel(payload: any) {
    const url = "api/channel/insertChannel";
    return this.service.basePost(url, payload);
  }

  updateChannel(payload: any) {
    const url = "api/channel/updateChannel";
    return this.service.basePost(url, payload);
  }

  getChannel(id: string) {
    const url = "api/channel/getChannel/" + id;
    return this.service.baseGet(url);
  }

  deleteChannel(payload: any) {
    console.log(payload);
    const url = "api/channel/deleteChannel";
    return this.service.basePost(url, payload);
  }
}
