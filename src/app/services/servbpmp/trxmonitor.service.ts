import { Injectable } from "@angular/core";
import { BackendService } from "../backend.service";

@Injectable({
  providedIn: "root",
})
export class TrxmonitorService {
  constructor(private service: BackendService) {}

  getAllTrxMon() {
    const url = "api/trx/getTransaction";
    return this.service.baseGet(url);
  }
  postParam(payload: any) {
    // const url = 'komi/montrx/getbyparam';
    const url = "api/trx/getTransactionByParam";
    return this.service.basePost(url, payload);
  }
}
