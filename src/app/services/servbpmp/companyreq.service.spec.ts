import { TestBed } from '@angular/core/testing';

import { CompanyreqService } from './companyreq.service';

describe('CompanyreqService', () => {
  let service: CompanyreqService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CompanyreqService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
