import { TestBed } from '@angular/core/testing';

import { ChannelreqService } from './channelreq.service';

describe('ChannelreqService', () => {
  let service: ChannelreqService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChannelreqService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
