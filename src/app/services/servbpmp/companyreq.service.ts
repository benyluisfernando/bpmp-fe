import { Injectable } from "@angular/core";
import { BackendService } from "../backend.service";

@Injectable({
  providedIn: "root",
})
export class CompanyreqService {
  constructor(private service: BackendService) {}

  getAllCompany() {
    const url = "api/company/getCompanyTemp";
    return this.service.baseGet(url);
  }

  getCompany(id: string) {
    const url = "api/company/getCompanyTemp/" + id;
    return this.service.baseGet(url);
  }

  approveCompany(payload: any) {
    const url = "api/company/approveCompanyTemp";
    return this.service.basePost(url, payload);
  }

  rejectCompany(payload: any) {
    const url = "api/company/rejectCompanyTemp";
    return this.service.basePost(url, payload);
  }
}
