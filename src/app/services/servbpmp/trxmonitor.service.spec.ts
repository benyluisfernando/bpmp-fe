import { TestBed } from '@angular/core/testing';

import { TrxmonitorService } from './trxmonitor.service';

describe('TrxmonitorService', () => {
  let service: TrxmonitorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrxmonitorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
