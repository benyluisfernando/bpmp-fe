import { Injectable } from "@angular/core";
import { BackendService } from "../backend.service";

@Injectable({
  providedIn: "root",
})
export class RulesService {
  constructor(private service: BackendService) {}

  getAllRules() {
    const url = "api/rules/getRules";
    return this.service.baseGet(url);
  }

  insertRules(payload: any) {
    const url = "api/rules/insertRules";
    return this.service.basePost(url, payload);
  }

  updateRules(payload: any) {
    const url = "api/rules/updateRules";
    return this.service.basePost(url, payload);
  }

  getRules(id: string) {
    const url = "api/rules/getRules/" + id;
    return this.service.baseGet(url);
  }

  deleteRules(payload: any) {
    console.log(payload);
    const url = "api/rules/deleteRules/" + payload;
    return this.service.baseGet(url);
  }
}
