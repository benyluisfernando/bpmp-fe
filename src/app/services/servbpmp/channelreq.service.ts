import { Injectable } from "@angular/core";
import { BackendService } from "../backend.service";

@Injectable({
  providedIn: "root",
})
export class ChannelreqService {
  constructor(private service: BackendService) {}

  getAllChannel() {
    const url = "api/channel/getChannelTemp";
    return this.service.baseGet(url);
  }

  getChannel(id: string) {
    const url = "api/channel/getChannelTemp/" + id;
    return this.service.baseGet(url);
  }

  approveChannel(payload: any) {
    const url = "api/channel/approveChannelTemp";
    return this.service.basePost(url, payload);
  }

  rejectChannel(payload: any) {
    const url = "api/channel/rejectChannelTemp";
    return this.service.basePost(url, payload);
  }
}
