import { Injectable } from "@angular/core";
import { BackendService } from "../backend.service";

@Injectable({
  providedIn: "root",
})
export class CompanyService {
  constructor(private service: BackendService) {}

  getAllCompany() {
    const url = "api/company/getCompany";
    return this.service.baseGet(url);
  }

  insertCompany(payload: any) {
    const url = "api/company/insertCompany";
    return this.service.basePost(url, payload);
  }

  updateCompany(payload: any) {
    const url = "api/company/updateCompany";
    return this.service.basePost(url, payload);
  }

  getCompany(id: string) {
    const url = "api/company/getCompany/" + id;
    return this.service.baseGet(url);
  }

  deleteCompany(payload: any) {
    console.log(payload);
    const url = "api/company/deleteCompany";
    return this.service.basePost(url, payload);
  }
}
