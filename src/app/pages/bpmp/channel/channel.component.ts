import { Component, OnInit } from "@angular/core";
import { MenuItem, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { BackendResponse } from "src/app/interfaces/backend-response";
import { BackendService } from "src/app/services/backend.service";
import { AuthService } from "src/app/services/auth.service";
import { OrganizationService } from "src/app/services/root/organization.service";
import { ChannelService } from "src/app/services/servbpmp/channel.service";

@Component({
  selector: "app-channel",
  templateUrl: "./channel.component.html",
  styleUrls: ["./channel.component.scss"],
})
export class ChannelComponent implements OnInit {
  viewApprove = false;
  viewDisplay = false;
  display = false;
  selectedChannel: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = "";
  orgheader: any = [
    { label: "Channel Id", sort: "CHANNEL_ID" },
    { label: "Channel Name", sort: "CHANNEL_NAME" },
    { label: "Channel Code", sort: "CHANNEL_CODE" },
    { label: "Merchant Type", sort: "MERCHANT_TYPE" },
    { label: "Create Who", sort: "CREATE_WHO" },
    { label: "Create Date", sort: "CREATE_DATE" },
    { label: "Change Who", sort: "CHANGE_WHO" },
    { label: "Change Date", sort: "CHANGE_DATE" },
  ];

  orgcolname: any = [
    "CHANNEL_ID",
    "CHANNEL_NAME",
    "CHANNEL_CODE",
    "MERCHANT_TYPE",
    "CREATE_WHO",
    "CREATE_DATE",
    "CHANGE_WHO",
    "CHANGE_DATE",
  ];
  orgcolhalign: any = ["", "", "", "", "", ""];
  orgcolwidth: any = [{ width: "110px" }, "", "", { width: "120px" }, ""];
  orgactionbtn: any = [1, 1, 1, 1, 1, 1, 1, 1];
  orgaddbtn = { route: "detail", label: "Add Data" };
  channellist: any[] = [];
  channellist2: any[] = [];

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private backend: BackendService,
    private channelService: ChannelService
  ) {}

  ngOnInit(): void {
    this.home = { icon: "pi pi-home", routerLink: "/" };
    this.breadcrumbs = [{ label: "Channel" }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingApp();
  }

  refreshingApp() {
    this.isFetching = true;
    this.channelService.getAllChannel().subscribe((result: BackendResponse) => {
      this.channellist2 = [];

      if (result.status === 202) {
        this.channellist = [];
        let objtmp = {
          CHANNEL_ID: "No records",
          CHANNEL_NAME: "No records",
          CHANNEL_CODE: "No records",
          MERCHANT_TYPE: "No records",
          CREATE_WHO: "No records",
          CREATE_DATE: "No records",
          CHANGE_WHO: "No records",
          CHANGE_DATE: "No records",
        };
        this.channellist.push(objtmp);
      } else {
        this.channellist = result.data;
      }
      this.isFetching = false;
    });
  }

  viewData(data: any) {
    console.log(data);
    this.viewDisplay = true;
    this.selectedChannel = data;
  }

  deleteConfirmation(data: any) {
    console.log("Di Emit nih dari child " + JSON.stringify(data));
    this.display = true;
    this.selectedChannel = data;
  }

  deleteChannel() {
    let channel = this.selectedChannel;
    const payload = { channel };
    this.channelService
      .deleteChannel(payload.channel)
      .subscribe((resp: BackendResponse) => {
        console.log(resp);
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshingApp();
      });
  }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: "success",
      summary: "Deleted",
      detail: message,
    });
  }
}
