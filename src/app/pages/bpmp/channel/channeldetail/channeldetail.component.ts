import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { MenuItem, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { AuthService } from "src/app/services/auth.service";
import { Location } from "@angular/common";
import { BackendResponse } from "src/app/interfaces/backend-response";
import { GroupServiceService } from "src/app/services/root/group-service.service";
import { ChannelService } from "src/app/services/servbpmp/channel.service";
import * as moment from "moment";

@Component({
  selector: "app-channeldetail",
  templateUrl: "./channeldetail.component.html",
  styleUrls: ["./channeldetail.component.scss"],
})
export class ChanneldetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  stateOptionsEdit: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  tokenID: string = "";
  groupForm!: FormGroup;
  submitted = false;
  orgsData: any[] = [];
  appInfoActive: any = {};
  orgSuggest: any = {};
  user: any = {};
  formatedOrg: any[] = [];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private groupService: GroupServiceService,
    private messageService: MessageService,
    private location: Location,
    private channelService: ChannelService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf("%23") !== -1 ? true : false;
    // console.log(">>>>>>>>>>> " + this.extraInfo);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { icon: "pi pi-home", routerLink: "/mgm/home" };
    this.breadcrumbs = [
      {
        label: "Channel Management",
        command: (event) => {
          this.location.back();
        },
        url: "",
      },
      { label: this.isEdit ? "Edit data" : "Add data" },
    ];

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      this.groupForm = this.formBuilder.group({
        channelId: ["", Validators.required],
        merchantType: ["", Validators.required],
        channelName: ["", Validators.required],
        createWho: ["", Validators.required],
        createDate: ["", Validators.required],
        changeWho: ["", Validators.required],
        changeDate: ["", Validators.required],
        channelCode: ["", Validators.required],
      });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get("id")) {
          this.userId = this.activatedRoute.snapshot.paramMap.get("id");
          this.channelService
            .getChannel(this.userId)
            .subscribe(async (result: BackendResponse) => {
              // console.log("Data edit user " + JSON.stringify(result.data));

              let cDate = result.data.CREATE_DATE;
              var createDate = moment(cDate).utc().format("YYYY-MM-DD");
              let chDate = result.data.CHANGE_DATE;
              var changeDate = moment(chDate).utc().format("YYYY-MM-DD");

              this.user.channelId = result.data.CHANNEL_ID;
              this.user.merchantType = result.data.MERCHANT_TYPE;
              this.user.channelName = result.data.CHANNEL_NAME;
              this.user.createWho = result.data.CREATE_WHO;
              this.user.createDate = createDate;
              this.user.changeWho = result.data.CHANGE_WHO;
              this.user.changeDate = changeDate;
              this.user.channelCode = result.data.CHANNEL_CODE;
              this.groupForm.patchValue({
                channelId: this.user.channelId,
                merchantType: this.user.merchantType,
                channelName: this.user.channelName,
                createWho: this.user.createWho,
                createDate: this.user.createDate,
                changeWho: this.user.changeWho,
                changeDate: this.user.changeDate,
                channelCode: this.user.channelCode,
              });
              this.groupForm.controls["channelId"].disable();
              console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  // formatOrgData(data: any) {
  //   data.map((dt: any) => {
  //     let formated: any = {};
  //     formated.name = dt.groupname;
  //     formated.id = dt.id;
  //     this.formatedOrg.push(formated);
  //   });
  // }

  // filterOrg(event: any) {
  //   let filtered: any[] = [];
  //   let query = event.query;

  //   for (let i = 0; i < this.formatedOrg.length; i++) {
  //     let country = this.formatedOrg[i];
  //     if (country.name.toLowerCase().indexOf(query.toLowerCase()) > -1) {
  //       filtered.push(country);
  //     }
  //   }

  //   this.orgSuggest = filtered;
  // }

  onSubmit() {
    this.submitted = true;
    // console.log(">>>>>>>>IS NULL >>>> " + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      var groupacl = this.groupForm.get("orgobj")?.value;
      let payload = {};

      if (!this.isEdit) {
        payload = {
          CHANNEL_ID: this.groupForm.get("channelId")?.value,
          MERCHANT_TYPE: this.groupForm.get("merchantType")?.value,
          CHANNEL_NAME: this.groupForm.get("channelName")?.value,
          CREATE_WHO: this.groupForm.get("createWho")?.value,
          CREATE_DATE: this.groupForm.get("createDate")?.value,
          CHANGE_WHO: this.groupForm.get("changeWho")?.value,
          CHANGE_DATE: this.groupForm.get("changeDate")?.value,
          CHANNEL_CODE: this.groupForm.get("channelCode")?.value,
        };

        console.log(">>>>>>>> payload " + JSON.stringify(payload));
        this.channelService.insertChannel(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status === 200) {
              this.location.back();
            }
          },
          (err) => {
            console.log(err);
            this.showTopCenterErr(err.error.data);
          }
        );
      } else {
        payload = {
          id: this.userId,
          CHANNEL_ID: this.groupForm.get("channelId")?.value,
          MERCHANT_TYPE: this.groupForm.get("merchantType")?.value,
          CHANNEL_NAME: this.groupForm.get("channelName")?.value,
          CREATE_WHO: this.groupForm.get("createWho")?.value,
          CREATE_DATE: this.groupForm.get("createDate")?.value,
          CHANGE_WHO: this.groupForm.get("changeWho")?.value,
          CHANGE_DATE: this.groupForm.get("changeDate")?.value,
          CHANNEL_CODE: this.groupForm.get("channelCode")?.value,
        };
        console.log(">>>>>>>> payload " + JSON.stringify(payload));
        this.channelService
          .updateChannel(payload)
          .subscribe((result: BackendResponse) => {
            if (result.status === 200) {
              this.location.back();
            }
          });
      }
    }
    // console.log(this.groupForm.valid);
  }
  showTopCenterErr(message: string) {
    this.messageService.add({
      severity: "error",
      summary: "Error",
      detail: message,
    });
  }
}
