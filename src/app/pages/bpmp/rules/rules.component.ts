import { Component, OnInit } from "@angular/core";
import { MenuItem, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { BackendResponse } from "src/app/interfaces/backend-response";
import { BackendService } from "src/app/services/backend.service";
import { AuthService } from "src/app/services/auth.service";
import { RulesService } from "src/app/services/servbpmp/rules.service";

@Component({
    selector: "app-rules",
    templateUrl: "./rules.component.html",
    styleUrls: ["./rules.component.scss"],
})
export class RulesComponent implements OnInit {
    viewDisplay = false;
    display = false;
    selectedRules: any = [];
    breadcrumbs!: MenuItem[];
    home!: MenuItem;
    isFetching: boolean = false;
    userInfo: any = {};
    tokenID: string = "";
    orgheader: any = [
        { label: "Rule ID", sort: "ruleId" },
        { label: "Channel ID", sort: "channelId" },
        { label: "Company ID", sort: "companyId" },
        { label: "Rule No", sort: "ruleNo" },
        { label: "Rule Type", sort: "ruleType" },
        { label: "Create Who", sort: "createdWho" },
        { label: "Create Date", sort: "createdDate" },
    ];
    orgcolname: any = [
        "RULE_ID",
        "CHANNEL_ID",
        "COMPANY_ID",
        "RULE_NO",
        "RULE_TYPE",
        "CREATE_WHO",
        "CREATE_DATE",
    ];
    orgcolhalign: any = [
        "p-text-center",
        "",
        "",
        "p-text-center",
        "p-text-center",
    ];
    orgcolwidth: any = [{ width: "110px" }, "", "", { width: "120px" }, ""];
    orgactionbtn: any = [1, 1, 1, 1, 1];
    orgaddbtn = { route: "detail", label: "Add Data" };
    ruleslist: any[] = [];

    constructor(
        private authservice: AuthService,
        public dialogService: DialogService,
        public messageService: MessageService,
        private backend: BackendService,
        private rulesService: RulesService
    ) {}

    ngOnInit(): void {
        this.home = { icon: "pi pi-home", routerLink: "/" };
        this.breadcrumbs = [{ label: "Rules" }];
        this.authservice.whoAmi().subscribe((value) => {
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
        });
        this.refreshingApp();
    }

    refreshingApp() {
        this.isFetching = true;
        this.rulesService.getAllRules().subscribe((result: BackendResponse) => {
            if (result.status === 202) {
                this.ruleslist = [];
                let objtmp = {
                    RULE_ID: "No records",
                    CHANNEL_ID: "No records",
                    COMPANY_ID: "No records",
                    RULE_NO: "No records",
                    CREATE_WHO: "No records",
                    CREATE_DATE: "No records",
                };
                this.ruleslist.push(objtmp);
            } else {
                this.ruleslist = result.data;
            }
            this.isFetching = false;
        });
    }

    viewData(data: any) {
        console.log("View" + JSON.stringify(data));
        this.viewDisplay = true;
        this.selectedRules = data;
    }

    deleteConfirmation(data: any) {
        console.log("Di Emit nih dari child " + JSON.stringify(data));
        this.display = true;
        this.selectedRules = data;
    }

    deleteRules() {
        console.log(this.selectedRules);
        let rules = this.selectedRules;
        const payload = { rules };
        this.rulesService
            .deleteRules(payload.rules.id)
            .subscribe((resp: BackendResponse) => {
                console.log(resp);
                if (resp.status === 200) {
                    this.showTopSuccess(resp.data);
                }
                this.display = false;
                this.refreshingApp();
            });
    }

    showTopSuccess(message: string) {
        this.messageService.add({
            severity: "success",
            summary: "Deleted",
            detail: message,
        });
    }
}
