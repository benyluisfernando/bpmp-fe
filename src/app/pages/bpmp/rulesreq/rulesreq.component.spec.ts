import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RulesreqComponent } from './rulesreq.component';

describe('RulesreqComponent', () => {
  let component: RulesreqComponent;
  let fixture: ComponentFixture<RulesreqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RulesreqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RulesreqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
