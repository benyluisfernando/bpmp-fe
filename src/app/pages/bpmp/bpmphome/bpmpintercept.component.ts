import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { SessionStorageService } from "angular-web-storage";
import { AuthService } from "src/app/services/auth.service";

@Component({
  selector: "app-bpmphome",
  template: "<h1>Hallo</h1>",
})
export class BpmpinterceptComponent implements OnInit {
  userInfo: any = {};
  //  app:any = {};
  //  applicationAvailable:number = 0;
  tokenID: string = "";
  param: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authservice: AuthService,
    private sessionStorage: SessionStorageService
  ) {}

  ngOnInit(): void {
    this.param = this.route.snapshot.params["config"];
    // console.log('>ITCEPT!@@!@!@!@!@>'+this.param);
    this.tokenID = this.param;
    // this.authservice.whoAmi().subscribe((value) =>{
    //   // console.log(">>> User Info : "+JSON.stringify(value));
    //     this.userInfo = value.data;
    //     console.log(">>> User Info : "+JSON.stringify(this.userInfo));
    //     this.tokenID = value.tokenId;

    // });

    this.authservice.whoAmiInitialize(this.tokenID).subscribe((value) => {
      // this.sessionStorage.set("accesstoken", authToken);
      // this.authservice.setAuthStatus(true);
      // this.authservice.setToken(authToken);
      console.log(">>> Token session : " + JSON.stringify(value));
      if ((value.status = 200)) {
        this.sessionStorage.set("accesstoken", value.data);
        this.authservice.setAuthStatus(true);
        this.authservice.setToken(value.data);
        this.router.navigate(["mgm/home"]);
      } else {
        this.router.navigate(["auth/login"]);
      }
    });
  }
}
