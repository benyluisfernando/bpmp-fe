import { ComponentFixture, TestBed } from "@angular/core/testing";

import { BpmphomeComponent } from "./bpmphome.component";

describe("PtbchomeComponent", () => {
  let component: BpmphomeComponent;
  let fixture: ComponentFixture<BpmphomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BpmphomeComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BpmphomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
