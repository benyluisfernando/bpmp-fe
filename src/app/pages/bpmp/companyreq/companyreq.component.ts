// import { Component, OnInit } from "@angular/core";
// import { MenuItem, MessageService } from "primeng/api";
// import { DialogService } from "primeng/dynamicdialog";
// import { BackendResponse } from "src/app/interfaces/backend-response";
// import { BackendService } from "src/app/services/backend.service";
// import { AuthService } from "src/app/services/auth.service";
// import { OrganizationService } from "src/app/services/root/organization.service";

// @Component({
//   selector: "app-companyreq",
//   templateUrl: "./companyreq.component.html",
//   styleUrls: ["./companyreq.component.scss"],
// })
// export class CompanyreqComponent implements OnInit {
//   display = false;
//   selectedOrganization: any = [];
//   breadcrumbs!: MenuItem[];
//   home!: MenuItem;
//   isFetching: boolean = false;
//   userInfo: any = {};
//   tokenID: string = "";
//   orgheader: any = [
//     { label: "Company ID", sort: "companyid" },
//     { label: "Company Code", sort: "companycode" },
//     { label: "CIF Code", sort: "companyname" },
//     { label: "Company Name", sort: "hostcode" },
//     { label: "Company Short Name", sort: "createdwho" },
//   ];
//   orgcolname: any = [
//     "COMPANY_ID",
//     "COMPANY_CODE",
//     "CIF_CODE",
//     "COMPANY_NAME",
//     "COMPANY_SHORTNAME",
//   ];
//   orgcolhalign: any = [
//     "p-text-center",
//     "",
//     "",
//     "p-text-center",
//     "p-text-center",
//   ];
//   orgcolwidth: any = [{ width: "110px" }, "", "", { width: "120px" }, ""];
//   orgactionbtn: any = [1, 1, 1, 1, 1];
//   orgaddbtn = { route: "detail", label: "Add Data" };
//   companyreqlist: any[] = [];

//   constructor(
//     private authservice: AuthService,
//     public dialogService: DialogService,
//     public messageService: MessageService,
//     private backend: BackendService,
//     private organizationService: OrganizationService
//   ) {}
//   ngOnInit(): void {
//     this.home = { icon: "pi pi-home", routerLink: "/" };
//     this.breadcrumbs = [{ label: "Pending Company" }];
//     // this.authservice.whoAmi().subscribe((value) =>{
//     //   // console.log(">>> User Info : "+JSON.stringify(value));
//     //     this.userInfo = value.data;
//     //     this.tokenID = value.tokenId;
//     // });
//     this.refreshingApp();
//   }
//   refreshingApp() {
//     this.isFetching = true;
//     this.backend
//       .basePost(
//         // 'restv2/billpayment.services.portal.ws:getCompany/getCompany',
//         "api/companyreq/getCompanyTemp",
//         {},
//         false
//       )
//       .subscribe((data: BackendResponse) => {
//         //console.log("Company length>>>>>>> " + JSON.stringify(data));
//         // if ((data.status = 200)) {
//         //   this.organizationService
//         //     .retriveOrgByTenant()
//         //     .subscribe((orgall: BackendResponse) => {
//         //console.log('>>>>>>> ' + data.data.data.length);
//         this.companyreqlist = data.data.data;

//         if (this.companyreqlist.length < 1) {
//           let objtmp = {
//             orgcode: "No records",
//             orgname: "No records",
//             orgdescription: "No records",
//             application: "No records",
//             created_by: "No records",
//           };
//           this.companyreqlist = [];
//           this.companyreqlist.push(objtmp);
//         }

//         this.isFetching = false;
//         //     });
//         // }
//       });
//   }
//   deleteConfirmation(data: any) {
//     console.log("Di Emit nih dari child " + JSON.stringify(data));
//     this.display = true;
//     this.selectedOrganization = data;
//   }
//   deleteOrganization() {
//     console.log(this.selectedOrganization);
//     let organization = this.selectedOrganization;
//     const payload = { organization };
//     this.organizationService
//       .deleteOrg(payload)
//       .subscribe((resp: BackendResponse) => {
//         console.log(resp);
//         if (resp.status === 200) {
//           this.showTopSuccess(resp.data);
//         }
//         this.display = false;
//         this.refreshingApp();
//       });
//   }
//   showTopSuccess(message: string) {
//     this.messageService.add({
//       severity: "success",
//       summary: "Deleted",
//       detail: message,
//     });
//   }
// }


import { Component, OnInit } from "@angular/core";
import { MenuItem, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { BackendResponse } from "src/app/interfaces/backend-response";
import { AuthService } from "src/app/services/auth.service";
import { CompanyreqService } from "src/app/services/servbpmp/companyreq.service";
import { AclmenucheckerService } from "src/app/services/utils/aclmenuchecker.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-companyreq",
  templateUrl: "./companyreq.component.html",
  styleUrls: ["./companyreq.component.scss"],
})
export class CompanyreqComponent implements OnInit {
  viewApprove = false;
  viewDisplay = false;
  display = false;
  selectedCompanyReq: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = "";

  orgheader: any = [
    { label: "Company ID", sort: "id" },
    { label: "Host Code", sort: "hostcode" },
    { label: "Company Name", sort: "companyName" },
    { label: "Created Who", sort: "createdWho" },
    { label: "Created Date", sort: "createdDate" },
    { label: "Company Code", sort: "companyCode" },
  ];

  orgcolname: any = [
    "COMPANY_ID",
    "HOST_CODE",
    "COMPANY_NAME",
    "CREATE_WHO",
    "CREATE_DATE",
    "COMPANY_CODE",
  ];
  orgcolhalign: any = [
    "p-text-center",
    "",
    "",
    "p-text-center",
    "p-text-center",
    "",
  ];
  orgcolwidth: any = [{ width: "110px" }, "", "", { width: "120px" }, ""];
  orgactionbtn: any = [0, 1, 0, 0, 0, 1, 1]; //[1, 1, 1, 1, 1];
  orgaddbtn = { route: "detail", label: "Add Data" };
  companyreqlist: any[] = [];

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private companyreqService: CompanyreqService,
    private aclMenuService: AclmenucheckerService,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.home = { icon: "pi pi-home", routerLink: "/" };
    this.breadcrumbs = [{ label: "Pending Company" }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then((data) => {
        console.log("MENU ALL ACL Set");
        this.aclMenuService.getAclMenu(this.router.url).then((dataacl) => {
          if (JSON.stringify(dataacl.acl) === "{}") {
            console.log("No ACL Founded");
          } else {
            console.log("ACL Founded");
            console.log(dataacl.acl.apprioval);
            this.orgactionbtn[0] = dataacl.acl.create;
            this.orgactionbtn[1] = dataacl.acl.read;
            this.orgactionbtn[2] = dataacl.acl.update;
            this.orgactionbtn[3] = dataacl.acl.delete;
            this.orgactionbtn[4] = dataacl.acl.view;
            this.orgactionbtn[5] = dataacl.acl.approval;
          }
        });
      });
    });
    this.refreshingApp();
  }
  refreshingApp() {
    this.isFetching = true;
    this.companyreqService
      .getAllCompany()
      .subscribe((result: BackendResponse) => {
        if (result.status === 202) {
          this.companyreqlist = [];
          let objtmp = {
            COMPANY_ID: "No records",
            COMPANY_NAME: "No records",
            COMPANY_CODE: "No records",
            HOST_CODE: "No records",
            CREATE_WHO: "No records",
            CREATE_DATE: "No records",
            CHANGE_WHO: "No records",
            CHANGE_DATE: "No records",
          };
          this.companyreqlist.push(objtmp);
        } else {
          this.companyreqlist = result.data;
        }
        this.isFetching = false;
      });
  }

  viewData(data: any) {
    console.log(data.idapproval);
    this.companyreqService
      .getCompany(data.idapproval)
      .subscribe((result: BackendResponse) => {
        if (result.status === 200) {
          console.log(result);

          this.viewDisplay = true;
          this.selectedCompanyReq = result.data;
        }
      });
    // this.viewDisplay = true;
    // this.selectedChannelReq = data;
  }
  approvalData(data: any) {
    // console.log(data);
    this.viewApprove = true;
    this.selectedCompanyReq = data;
  }

  approvalSubmit(status) {
    let payload = {
      id: this.selectedCompanyReq.id,
      oldactive: this.selectedCompanyReq.active,
      isactive: status,
      idapproval: this.selectedCompanyReq.idapproval,
    };
    // console.log(">>>>>>>> payload " + JSON.stringify(payload));
    if (status == 4) {
      this.companyreqService
        .rejectCompany(payload)
        .subscribe((result: BackendResponse) => {
          if (result.status === 200) {
            this.refreshingApp();
            this.viewApprove = false;
          }
        });
    } else {
      this.companyreqService
        .approveCompany(payload)
        .subscribe((result: BackendResponse) => {
          if (result.status === 200) {
            this.refreshingApp();
            this.viewApprove = false;
          }
        });
    }
  }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: "success",
      summary: "Deleted",
      detail: message,
    });
  }
}
