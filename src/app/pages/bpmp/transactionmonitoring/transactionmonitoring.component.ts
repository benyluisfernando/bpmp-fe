import { Component, OnInit, ViewChild } from "@angular/core";
import { MenuItem, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { DatePipe } from "@angular/common";
// import { Table } from 'primeng/table';
import { AuthService } from "src/app/services/auth.service";
import { TrxmonitorService } from "src/app/services/servbpmp/trxmonitor.service";
import { BackendResponse } from "src/app/interfaces/backend-response";
import * as XLSX from "xlsx";
import jsPDF from "jspdf";
import autoTable from "jspdf-autotable";
import { element } from "protractor";
// import { SysparamService } from 'src/app/services/komi/sysparam.service';
import { MainmenulayoutComponent } from "../../../layout/mainmenulayout/mainmenulayout.component";

@Component({
  selector: "app-transactionmonitoring",
  templateUrl: "./transactionmonitoring.component.html",
  styleUrls: ["./transactionmonitoring.component.scss"],
})
export class TransactionmonitoringComponent implements OnInit {
  rangeDates: Date[];
  isdataexist = false;
  isactives: any = [];
  isactivesSelected: any = {};
  channels: any = [];
  channelSelected: any = {};
  logUbpTrxId: any;
  transactiontype: any = [];
  transactiontypeSelected: any = {};
  fileName = "ExcelSheet.xlsx";
  startDate: any = new Date();
  endDate: any;
  display = false;
  displayPrv = false;
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;
  userInfo: any = {};
  tokenID: string = "";
  logData: any[] = [];
  dateTime: any;
  dateNow: any;
  birthday: any;
  isLoggedIn: boolean = false;
  sysParamData: any[] = [];
  range: number = 14;
  jsonUserInfo: any = {};
  username: string = "Unknown";
  div: string = "   ";
  // no: number = 1;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    public datepipe: DatePipe,
    private trxmonitorService: TrxmonitorService,
    // private sysparamService: SysparamService,
    private ServUsername: MainmenulayoutComponent
  ) {}

  ngOnInit(): void {
    this.home = { icon: "pi pi-home", routerLink: "/mgm/home" };
    this.breadcrumbs = [{ label: "Transaction Monitoring" }];

    // mantap
    this.isactives = [
      { label: "Sukses", value: "S" },
      { label: "Error", value: "E" },
      { label: "Timeout", value: "T" },
    ];

    this.transactiontype = [
      { label: "Incoming Transaction", value: "I" },
      { label: "Outgoing Transaction", value: "O" },
    ];
    this.logData = [];

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.initialData();
  }

  refreshData() {
    this.isdataexist = false;
    this.logData = [];
    this.isFetching = true;
    let payload: any = {};
    // if (this.rangeDates != undefined) {
    //   start_date = this.datepipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
    //   end_date = this.datepipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
    // }
    // if (start_date) payload.start_date = start_date;
    // if (end_date) payload.end_date = end_date;

    if (this.startDate) {
      payload.startDate = this.datepipe.transform(this.startDate, "yyyy-MM-dd");
    }
    this.dateTime = new Date(
      this.startDate.getFullYear(),
      this.startDate.getMonth(),
      this.startDate.getDate() + Number(this.range)
    );

    if (this.endDate > this.dateTime) {
      this.isLoggedIn = true;
      this.endDate = this.dateTime;
      payload.endDate = this.datepipe.transform(this.dateTime, "yyyy-MM-dd");
    } else if (this.endDate == null) {
      this.isLoggedIn = false;
      this.endDate = this.dateTime;
      payload.endDate = this.datepipe.transform(this.dateTime, "yyyy-MM-dd");
    } else {
      this.isLoggedIn = false;
      payload.endDate = this.datepipe.transform(this.endDate, "yyyy-MM-dd");
    }

    this.isLoggedIn = false;

    if (this.isactivesSelected) payload.status = this.isactivesSelected.value;
    if (this.channelSelected)
      payload.channel = this.channelSelected.channelcode;
    if (this.logUbpTrxId) payload.logUbpTrxId = this.logUbpTrxId;
    if (this.transactiontypeSelected)
      payload.trxtype = this.transactiontypeSelected.value;
    // console.log('JSON>>>', JSON.stringify(payload));

    this.trxmonitorService
      .postParam(payload)
      .subscribe((result: BackendResponse) => {
        console.log(JSON.stringify(result));
        console.log(result);
        if (result.status == 200) {
          this.isdataexist = true;
          this.logData = result.data;
        } else {
          this.isdataexist = false;
          this.logData = [];
          let objtmp = {
            id: "No record",
            LOG_UBP_TRX_ID: "No record",
            LOG_BILL_ID: "No record",
            EVENT: "No record",
            TRX_TIMESTAMP: "No record",
            BILL_KEY1: "No record",
            BILL_KEY2: "No record",
            BILL_KEY3: "No record",
            RESPONSE_CODE: "No record",
          };
          this.logData.push(objtmp);
        }
        this.isFetching = false;
      });
  }

  initialData() {
    this.isFetching = true;
    this.trxmonitorService
      .getAllTrxMon()
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>> " + JSON.stringify(result));
        if (result.status === 200) {
          // 202
          let objtmp = {
            id: "No record",
            LOG_UBP_TRX_ID: "No record",
            LOG_BILL_ID: "No record",
            EVENT: "No record",
            TRX_TIMESTAMP: "No record",
            BILL_KEY1: "No record",
            BILL_KEY2: "No record",
            BILL_KEY3: "No record",
            RESPONSE_CODE: "No record",
          };
          this.logData.push(objtmp);
        } else {
          this.isdataexist = true;
          this.logData = result.data.results;
        }
        this.isFetching = false;
      });
  }

  maxDate() {
    const now = this.startDate;
    this.birthday = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate() + Number(this.range)
    );

    var date = this.birthday,
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);

    this.birthday = [day, mnth, date.getFullYear()].join("-");
    this.isLoggedIn = true;
    return this.birthday;
  }

  functionAmount() {
    var total = 0;
    for (var i = 0; i < this.logData.length; i++) {
      var product = this.logData[i].trx_amount;
      total += Number(product);
    }
    return total;
  }

  functionVolume() {
    return this.logData.length;
  }

  exportexcel(): void {
    let myDate = new Date();
    let datenow = this.datepipe.transform(myDate, "yyyyMMddHHmmss");
    // this.fileName = 'xls' + datenow + '.xlsx';
    this.fileName = "csv" + datenow + ".csv";
    /* table id is passed over here */
    let element = document.getElementById("excel-table");
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: "success",
      summary: "Deleted",
      detail: message,
    });
  }
  openPDF(): void {
    var number = 1;
    var doc = new jsPDF("l", "mm", [297, 210]);
    var col = [
      [
        "NO",
        "UBP TRX ID",
        "BILL ID",
        "EVENT",
        "TRX TIMESTAMP",
        "BILL KEY1",
        "BILL KEY1",
        "BILL KEY1",
        "RESPONSE CODE",
      ],
    ];
    var rows = [];

    let myDate = new Date();
    let datenow = this.datepipe.transform(myDate, "yyyyMMddHHmmss");
    this.fileName = "pdf" + datenow + ".pdf";
    this.logData.forEach((element) => {
      let tid = "";

      // TRX_TIMESTAMP
      tid = this.datepipe.transform(
        element.TRX_TIMESTAMP,
        "yyyy-MM-dd HH:mm:ss"
      );

      var temp = [
        number++,
        element.LOG_UBP_TRX_ID,
        element.LOG_BILL_ID,
        element.EVENT,
        tid,
        element.BILL_KEY1,
        element.BILL_KEY2,
        element.BILL_KEY3,
        element.RESPONSE_CODE,
      ];
      rows.push(temp);
    });
    // doc.autoTable(col, rows);
    // doc.text('Left aligned text', 15, 10);

    //header right
    doc.setFontSize(8);
    let dateGenerated = this.datepipe.transform(myDate, "dd/MM/yyyy");
    let timeGenerated = this.datepipe.transform(myDate, "HH:mm:ss");
    doc.text(
      "DATE GENERATED :\t" + dateGenerated,
      doc.internal.pageSize.getWidth() - 60,
      20,
      {
        align: "left",
      }
    );
    doc.text(
      "TIME GENERATED : \t" + timeGenerated,
      doc.internal.pageSize.getWidth() - 60,
      25,
      {
        align: "left",
      }
    );
    doc.text(
      "GENERATED BY     :  \t" + this.ServUsername.getUsername(),
      doc.internal.pageSize.getWidth() - 60,
      30,
      {
        align: "left",
      }
    );

    // header page
    doc.setFontSize(12);
    doc.text(
      "TRANSFER TRANSACTION DETAIL REPORT",
      doc.internal.pageSize.getWidth() / 2,
      40,
      {
        align: "center",
      }
    );

    // range date
    doc.setFontSize(12);
    let stardate = this.datepipe.transform(this.startDate, "yyyy/MM/dd");
    let endate = this.datepipe.transform(this.endDate, "yyyy/MM/dd"); // note
    doc.text(
      "Date : " + stardate + " - " + endate,
      doc.internal.pageSize.getWidth() / 2,
      45,
      {
        align: "center",
      }
    );

    autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (rows) => {},
      headStyles: { fillColor: [128, 128, 128] },
      styles: { fontSize: 8 },
      // columnStyles: {
      //   1: { cellWidth: 25 },
      //   3: { cellWidth: 18 },
      //   5: { cellWidth: 18 },
      //   8: { cellWidth: 18 },
      //   9: { cellWidth: 19 },
      // },
      margin: { top: 70, right: 14, bottom: 10, left: 14 },
      didDrawPage: function (data) {
        data.settings.margin.top = 25;
      },
    });
    const pageCount = doc.internal.pages.length - 1;
    // console.log('pageCount', pageCount);
    for (var i = 1; i <= pageCount; i++) {
      doc.setPage(i);
      doc.text(
        "Page " + String(i) + " from " + String(pageCount),
        doc.internal.pageSize.getWidth() - 60,
        10
      );
    }
    doc.save(this.fileName);
  }

  removeFilter() {
    // this.startDate = null;
    this.startDate = new Date();
    // this.endDate = null;
    this.logUbpTrxId = "";
    this.endDate = null;
    this.isactivesSelected = [0];
    this.channelSelected = [0];
    this.transactiontypeSelected = [0];
    // this.refreshData();
    this.isLoggedIn = false;
  }
}
