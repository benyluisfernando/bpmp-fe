import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionmonitoringComponent } from './transactionmonitoring.component';

describe('TransactionmonitoringComponent', () => {
  let component: TransactionmonitoringComponent;
  let fixture: ComponentFixture<TransactionmonitoringComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionmonitoringComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionmonitoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
