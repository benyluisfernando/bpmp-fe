import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { MenuItem, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { AuthService } from "src/app/services/auth.service";
import { Location } from "@angular/common";
import { UsermanagerService } from "src/app/services/root/usermanager.service";
import { FilterService } from "primeng/api";
import { BackendResponse } from "src/app/interfaces/backend-response";
import { ForgotpasswordService } from "src/app/services/forgotpassword/forgotpassword.service";
import { CompanyService } from "src/app/services/servbpmp/company.service";
import * as moment from "moment";

@Component({
  selector: "app-companydetail",
  templateUrl: "./companydetail.component.html",
  styleUrls: ["./companydetail.component.scss"],
})
export class CompanydetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  stateOptionsEdit: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  tokenID: string = "";
  groupForm!: FormGroup;
  submitted = false;
  orgsData: any[] = [];
  appInfoActive: any = {};
  orgSuggest: any = {};
  user: any = {};
  formatedOrg: any[] = [];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private umService: UsermanagerService,
    private authservice: AuthService,
    private filterService: FilterService,
    private messageService: MessageService,
    private location: Location,
    private verifikasiService: ForgotpasswordService,
    private companyService: CompanyService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf("%23") !== -1 ? true : false;
    console.log(">>>>>>>>>>> " + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { icon: "pi pi-home", routerLink: "/mgm/home" };
    this.breadcrumbs = [
      {
        label: "Company Management",
        command: (event) => {
          this.location.back();
        },
        url: "",
      },
      { label: this.isEdit ? "Edit data" : "Add data" },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      this.groupForm = this.formBuilder.group({
        companyId: ["", Validators.required],
        companyCode: ["", Validators.required],
        companyName: ["", Validators.required],
        createWho: ["", Validators.required],
        createDate: ["", Validators.required],
        hostCode: ["", Validators.required],
      });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get("id")) {
          this.userId = this.activatedRoute.snapshot.paramMap.get("id");
          this.companyService
            .getCompany(this.userId)
            .subscribe(async (result: BackendResponse) => {
              console.log("Data edit user " + JSON.stringify(result.data));
              let cDate = result.data.CREATE_DATE;
              var createDate = moment(cDate).utc().format("YYYY-MM-DD");
              this.user.companyId = result.data.COMPANY_ID;
              this.user.companyCode = result.data.COMPANY_CODE;
              this.user.companyName = result.data.COMPANY_NAME;
              this.user.createWho = result.data.CREATE_WHO;
              this.user.createDate = createDate;
              this.user.hostCode = result.data.HOST_CODE;
              this.groupForm.patchValue({
                companyId: this.user.companyId,
                companyCode: this.user.companyCode,
                companyName: this.user.companyName,
                createWho: this.user.createWho,
                createDate: this.user.createDate,
                hostCode: this.user.hostCode,
              });
              this.groupForm.controls["companyId"].disable();
              console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      var groupacl = this.groupForm.get("orgobj")?.value;
      let payload = {};

      if (!this.isEdit) {
        payload = {
          companyId: this.groupForm.get("companyId")?.value,
          companyCode: this.groupForm.get("companyCode")?.value,
          companyName: this.groupForm.get("companyName")?.value,
          createWho: this.groupForm.get("createWho")?.value,
          createDate: this.groupForm.get("createDate")?.value,
          hostCode: this.groupForm.get("hostCode")?.value,
        };
        console.log(">>>>>>>> payload " + JSON.stringify(payload));
        this.companyService.insertCompany(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status === 200) {
              this.location.back();
            }
          },
          (err) => {
            console.log(err);
            this.showTopCenterErr(err.error.data);
          }
        );
      } else {
        payload = {
          id: this.userId,
          companyId: this.groupForm.get("companyId")?.value,
          companyCode: this.groupForm.get("companyCode")?.value,
          companyName: this.groupForm.get("companyName")?.value,
          createWho: this.groupForm.get("createWho")?.value,
          createDate: this.groupForm.get("createDate")?.value,
          hostCode: this.groupForm.get("hostCode")?.value,
        };
        console.log(">>>>>>>> payload " + JSON.stringify(payload));
        this.companyService
          .updateCompany(payload)
          .subscribe((result: BackendResponse) => {
            // console.log(">>>>>>>> return "+JSON.stringify(result));
            if (result.status === 200) {
              this.location.back();
            }
          });
      }
    }

    console.log(this.groupForm.valid);
  }

  showTopCenterErr(message: string) {
    this.messageService.add({
      severity: "error",
      summary: "Error",
      detail: message,
    });
  }
}
