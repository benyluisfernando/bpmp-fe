// import { Component, OnInit } from "@angular/core";
// import { MenuItem, MessageService } from "primeng/api";
// import { DialogService } from "primeng/dynamicdialog";
// import { BackendResponse } from "src/app/interfaces/backend-response";
// import { BackendService } from "src/app/services/backend.service";
// import { AuthService } from "src/app/services/auth.service";
// import { OrganizationService } from "src/app/services/root/organization.service";
// import { AclmenucheckerService } from "src/app/services/utils/aclmenuchecker.service";
// import { CompanyService } from "src/app/services/servbpmp/company.service";

// @Component({
//   selector: "app-company",
//   templateUrl: "./company.component.html",
//   styleUrls: ["./company.component.scss"],
// })
// export class CompanyComponent implements OnInit {
//   viewDisplay = false;
//   display = false;
//   selectedCompany: any = [];
//   breadcrumbs!: MenuItem[];
//   home!: MenuItem;
//   isFetching: boolean = false;
//   companyInfo: any = {};
//   userInfo: any = {};
//   tokenID: string = "";
//   orgheader: any = [
//     { label: "Company ID", sort: "COMPANY_ID" },
//     { label: "Company Code", sort: "COMPANY_CODE" },
//     { label: "Company Name", sort: "COMPANY_NAME" },
//     { label: "Host Code", sort: "HOST_CODE" },
//     { label: "Created Who", sort: "CREATE_WHO" },
//     { label: "Created Date", sort: "CREATE_DATE" },
//   ];
//   orgcolname: any = [
//     "COMPANY_ID",
//     "COMPANY_CODE",
//     "COMPANY_NAME",
//     "HOST_CODE",
//     "CREATE_WHO",
//     "CREATE_DATE",
//   ];
//   orgcolhalign: any = [
//     "p-text-center",
//     "p-text-center",
//     "p-text-center",
//     "p-text-center",
//     "p-text-center",
//   ];
//   orgcolwidth: any = [{ width: "110px" }, "", "", { width: "120px" }, ""];
//   orgactionbtn: any = [1, 1, 1, 1, 1];
//   orgaddbtn = { route: "detail", label: "Add Data" };
//   companylist: any[] = [];
//   router: any;

//   constructor(
//     private authservice: AuthService,
//     public dialogService: DialogService,
//     public messageService: MessageService,
//     private backend: BackendService,
//     private organizationService: OrganizationService,
//     private companyService: CompanyService
//   ) {}
//   ngOnInit(): void {
//     this.home = { icon: "pi pi-home", routerLink: "/" };
//     this.breadcrumbs = [{ label: "Company List" }];
//     this.authservice.whoAmi().subscribe((value) => {
//       this.userInfo = value.data;
//       this.tokenID = value.tokenId;
//     });
//     this.refreshingApp();
//   }
//   refreshingApp() {
//     this.isFetching = true;
//     this.companyService.getAllCompany().subscribe((result: BackendResponse) => {
//       if (result.status === 202) {
//         this.companylist = [];
//         let objtmp = {
//           COMPANY_ID: "No records",
//           COMPANY_CODE: "No records",
//           COMPANY_NAME: "No records",
//           CREATE_WHO: "No records",
//           CREATE_DATE: "No records",
//           HOST_CODE: "No records",
//         };
//         this.companylist.push(objtmp);
//       } else {
//         this.companylist = result.data;
//       }
//       this.isFetching = false;
//     });
//   }

//   viewData(data: any) {
//     console.log("View" + JSON.stringify(data));
//     this.viewDisplay = true;
//     this.selectedCompany = data;
//   }

//   deleteConfirmation(data: any) {
//     console.log("Delete Confrim " + JSON.stringify(data));
//     this.display = true;
//     this.selectedCompany = data;
//   }

//   deleteCompany() {
//     console.log(this.selectedCompany);
//     let company = this.selectedCompany;
//     const payload = { company };
//     this.companyService
//       .deleteCompany(payload.company.id)
//       .subscribe((resp: BackendResponse) => {
//         console.log(resp);
//         if (resp.status === 200) {
//           this.showTopSuccess(resp.data);
//         }
//         this.display = false;
//         this.refreshingApp();
//       });
//   }
//   showTopSuccess(message: string) {
//     this.messageService.add({
//       severity: "success",
//       summary: "Deleted",
//       detail: message,
//     });
//   }
// }


import { Component, OnInit } from "@angular/core";
import { MenuItem, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { BackendResponse } from "src/app/interfaces/backend-response";
import { BackendService } from "src/app/services/backend.service";
import { AuthService } from "src/app/services/auth.service";
import { OrganizationService } from "src/app/services/root/organization.service";
import { CompanyService } from "src/app/services/servbpmp/company.service";

@Component({
  selector: "app-company",
  templateUrl: "./company.component.html",
  styleUrls: ["./company.component.scss"],
})
export class CompanyComponent implements OnInit {
  viewApprove = false;
  viewDisplay = false;
  display = false;
  selectedCompany: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = "";
  orgheader: any = [
    { label: "Company Id", sort: "COMPANY_ID" },
    { label: "Company Name", sort: "COMPANY_NAME" },
    { label: "Company Code", sort: "COMPANY_CODE" },
    { label: "Host Code", sort: "MERCHANT_TYPE" },
    { label: "Create Who", sort: "CREATE_WHO" },
    { label: "Create Date", sort: "CREATE_DATE" },
  ];

  orgcolname: any = [
    "COMPANY_ID",
    "COMPANY_NAME",
    "COMPANY_CODE",
    "HOST_CODE",
    "CREATE_WHO",
    "CREATE_DATE",
  ];
  orgcolhalign: any = ["", "", "", "", "", ""];
  orgcolwidth: any = [{ width: "110px" }, "", "", { width: "120px" }, ""];
  orgactionbtn: any = [1, 1, 1, 1, 1, 1, 1, 1];
  orgaddbtn = { route: "detail", label: "Add Data" };
  companylist: any[] = [];
  companylist2: any[] = [];

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private backend: BackendService,
    private companyService: CompanyService
  ) {}

  ngOnInit(): void {
    this.home = { icon: "pi pi-home", routerLink: "/" };
    this.breadcrumbs = [{ label: "Company" }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingApp();
  }

  refreshingApp() {
    this.isFetching = true;
    this.companyService.getAllCompany().subscribe((result: BackendResponse) => {
      this.companylist2 = [];

      if (result.status === 202) {
        this.companylist = [];
        let objtmp = {
          COMPANY_ID: "No records",
          COMPANY_NAME: "No records",
          COMANY_CODE: "No records",
          HOST_CODE: "No records",
          CREATE_WHO: "No records",
          CREATE_DATE: "No records",
        };
        this.companylist.push(objtmp);
      } else {
        this.companylist = result.data;
      }
      this.isFetching = false;
    });
  }

  viewData(data: any) {
    console.log(data);
    this.viewDisplay = true;
    this.selectedCompany = data;
  }

  deleteConfirmation(data: any) {
    console.log("Di Emit nih dari child " + JSON.stringify(data));
    this.display = true;
    this.selectedCompany = data;
  }

  deleteCompany() {
    let company = this.selectedCompany;
    const payload = { company };
    this.companyService
      .deleteCompany(payload.company)
      .subscribe((resp: BackendResponse) => {
        console.log(resp);
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshingApp();
      });
  }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: "success",
      summary: "Deleted",
      detail: message,
    });
  }
}
