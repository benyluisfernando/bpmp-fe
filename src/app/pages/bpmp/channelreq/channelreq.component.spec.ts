import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelreqComponent } from './channelreq.component';

describe('ChannelreqComponent', () => {
  let component: ChannelreqComponent;
  let fixture: ComponentFixture<ChannelreqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelreqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelreqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
