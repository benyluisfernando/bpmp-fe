import { Component, OnInit } from "@angular/core";
import { MenuItem, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { BackendResponse } from "src/app/interfaces/backend-response";
import { AuthService } from "src/app/services/auth.service";
import { ChannelreqService } from "src/app/services/servbpmp/channelreq.service";
import { AclmenucheckerService } from "src/app/services/utils/aclmenuchecker.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-channelreq",
  templateUrl: "./channelreq.component.html",
  styleUrls: ["./channelreq.component.scss"],
})
export class ChannelreqComponent implements OnInit {
  viewApprove = false;
  viewDisplay = false;
  display = false;
  selectedChannelReq: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = "";

  orgheader: any = [
    { label: "Channel ID", sort: "id" },
    { label: "Merchant Type", sort: "channelId" },
    { label: "Channel Name", sort: "channelName" },
    { label: "Created Who", sort: "createdWho" },
    { label: "Created Date", sort: "createdDate" },
    { label: "Change Who", sort: "changedWho" },
    { label: "Channel Code", sort: "channelCode" },
    { label: "Change Date", sort: "changedDate" },
  ];

  orgcolname: any = [
    "CHANNEL_ID",
    "MERCHANT_TYPE",
    "CHANNEL_NAME",
    "CREATE_WHO",
    "CREATE_DATE",
    "CHANGE_WHO",
    "CHANNEL_CODE",
    "CHANGE_DATE",
  ];
  orgcolhalign: any = [
    "p-text-center",
    "",
    "",
    "p-text-center",
    "p-text-center",
    "",
  ];
  orgcolwidth: any = [{ width: "110px" }, "", "", { width: "120px" }, ""];
  orgactionbtn: any = [0, 1, 0, 0, 0, 1, 1]; //[1, 1, 1, 1, 1];
  orgaddbtn = { route: "detail", label: "Add Data" };
  channelreqlist: any[] = [];

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private channelreqService: ChannelreqService,
    private aclMenuService: AclmenucheckerService,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.home = { icon: "pi pi-home", routerLink: "/" };
    this.breadcrumbs = [{ label: "Pending Company" }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then((data) => {
        console.log("MENU ALL ACL Set");
        this.aclMenuService.getAclMenu(this.router.url).then((dataacl) => {
          if (JSON.stringify(dataacl.acl) === "{}") {
            console.log("No ACL Founded");
          } else {
            console.log("ACL Founded");
            console.log(dataacl.acl.apprioval);
            this.orgactionbtn[0] = dataacl.acl.create;
            this.orgactionbtn[1] = dataacl.acl.read;
            this.orgactionbtn[2] = dataacl.acl.update;
            this.orgactionbtn[3] = dataacl.acl.delete;
            this.orgactionbtn[4] = dataacl.acl.view;
            this.orgactionbtn[5] = dataacl.acl.approval;
          }
        });
      });
    });
    this.refreshingApp();
  }
  refreshingApp() {
    this.isFetching = true;
    this.channelreqService
      .getAllChannel()
      .subscribe((result: BackendResponse) => {
        if (result.status === 202) {
          this.channelreqlist = [];
          let objtmp = {
            CHANNEL_ID: "No records",
            CHANNEL_NAME: "No records",
            CHANNEL_CODE: "No records",
            MERCHANT_TYPE: "No records",
            CREATE_WHO: "No records",
            CREATE_DATE: "No records",
            CHANGE_WHO: "No records",
            CHANGE_DATE: "No records",
          };
          this.channelreqlist.push(objtmp);
        } else {
          this.channelreqlist = result.data;
        }
        this.isFetching = false;
      });
  }

  viewData(data: any) {
    console.log(data.idapproval);
    this.channelreqService
      .getChannel(data.idapproval)
      .subscribe((result: BackendResponse) => {
        if (result.status === 200) {
          console.log(result);

          this.viewDisplay = true;
          this.selectedChannelReq = result.data;
        }
      });
    // this.viewDisplay = true;
    // this.selectedChannelReq = data;
  }
  approvalData(data: any) {
    // console.log(data);
    this.viewApprove = true;
    this.selectedChannelReq = data;
  }

  approvalSubmit(status) {
    let payload = {
      id: this.selectedChannelReq.id,
      oldactive: this.selectedChannelReq.active,
      isactive: status,
      idapproval: this.selectedChannelReq.idapproval,
    };
    // console.log(">>>>>>>> payload " + JSON.stringify(payload));
    if (status == 4) {
      this.channelreqService
        .rejectChannel(payload)
        .subscribe((result: BackendResponse) => {
          if (result.status === 200) {
            this.refreshingApp();
            this.viewApprove = false;
          }
        });
    } else {
      this.channelreqService
        .approveChannel(payload)
        .subscribe((result: BackendResponse) => {
          if (result.status === 200) {
            this.refreshingApp();
            this.viewApprove = false;
          }
        });
    }
  }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: "success",
      summary: "Deleted",
      detail: message,
    });
  }
}
