import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-eventlogs',
  templateUrl: './eventlogs.component.html',
  styleUrls: ['./eventlogs.component.scss']
})
export class EventlogsComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  constructor() { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/'};
    this.breadcrumbs = [
      { label: 'Eventlogs Monitoring' }
    ];
  }

}
