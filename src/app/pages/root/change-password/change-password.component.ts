import { BoundDirectivePropertyAst } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { MessageService } from 'primeng/api';
import {
  DialogService,
  DynamicDialogConfig,
  DynamicDialogRef,
} from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { UsermanagerService } from 'src/app/services/root/usermanager.service';
//import { UsermanagerService } from 'src/app/services/usermanager.service';

interface ChangePassword {
  id?: string;
  oldpassword?: string;
  newpassword?: string;
}

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  inputForm!: FormGroup;
  changepass: ChangePassword = {};
  submitted = false;
  confirmed = false;
  err: any = {};
  constructor(
    private authservice: AuthService,
    public ref: DynamicDialogRef,
    private usermanager: UsermanagerService,
    private sessionStorage: SessionStorageService,
    public config: DynamicDialogConfig,
    public dialogService: DialogService,
    private route: Router,
    public messageService: MessageService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.inputForm = this.formBuilder.group({
      oldpassword: ['', Validators.required],
      newpassword: ['', Validators.required],
      confirmpassword: ['', Validators.required],
    });
  }

  get f() {
    return this.inputForm.controls;
  }
  get userFormControl() {
    return this.inputForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.inputForm.valid) {
      if (
        this.inputForm.value.newpassword == this.inputForm.value.confirmpassword
      ) {
        this.authservice.whoAmi().subscribe((data: BackendResponse) => {
          if ((data.status = 200)) {
            console.log(data.data);
            this.changepass.id = data.data.id;
            this.changepass.oldpassword = this.inputForm.value['oldpassword'];
            this.changepass.newpassword = this.inputForm.value['newpassword'];
            console.log(this.changepass);
            this.usermanager.putPassword(this.changepass).subscribe(
              (data: BackendResponse) => {
                if ((data.status = 200)) {
                  this.ref.close(data.status);
                } else {
                  this.ref.close(data.status);
                }
              },
              (error) => {
                this.err.msg = error.error.data;
              }
            );
          }
        });
      } else {
        this.confirmed = true;
      }
    }
  }

  onCancel() {
    this.ref.destroy();
  }
}
