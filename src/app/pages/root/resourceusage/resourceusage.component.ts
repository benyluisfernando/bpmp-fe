import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-resourceusage',
  templateUrl: './resourceusage.component.html',
  styleUrls: ['./resourceusage.component.scss']
})
export class ResourceusageComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  constructor() { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/'};
    this.breadcrumbs = [
      { label: 'Resource Usages' }
    ];
  }

}
