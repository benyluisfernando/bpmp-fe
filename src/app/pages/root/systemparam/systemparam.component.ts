import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MenuItem, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { BackendResponse } from "src/app/interfaces/backend-response";
import { AuthService } from "src/app/services/auth.service";
import { SysparamService } from "src/app/services/servbpmp/sysparam.service";
import { AclmenucheckerService } from "src/app/services/utils/aclmenuchecker.service";

@Component({
  selector: "app-systemparam",
  templateUrl: "./systemparam.component.html",
  styleUrls: ["./systemparam.component.scss"],
})
export class SystemparamComponent implements OnInit {
  display = false;
  viewApprove = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userlist: any[] = [];
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = "";
  usrheader: any = [
    { label: "No", sort: "id" },
    { label: "Parameter", sort: "param_name" },
    { label: "Value", sort: "param_value" },
    { label: "Category", sort: "category" },
    { label: "Status", sort: "active" },
    { label: "Description", sort: "description" },
  ];
  usrcolname: any = [
    "id",
    "param_name",
    "param_value",
    "category",
    "active",
    "description",
  ];
  usrcolhalign: any = [
    "p-text-center",
    "",
    "p-text-center",
    "",
    "p-text-center",
    "",
  ];
  usrcolwidth: any = [
    { width: "80px" },
    "",
    "",
    { width: "180px" },
    { width: "105px" },
    "",
  ];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  usractionbtn: any = [0, 1, 1, 0, 0, 1];
  usraddbtn = { route: "detail", label: "Add Data" };

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private sysService: SysparamService,
    private aclMenuService: AclmenucheckerService,
    private router: Router
  ) {}
  // private sysparamService:sysparamService

  ngOnInit(): void {
    this.home = { icon: "pi pi-home", routerLink: "/mgm/home" };
    this.breadcrumbs = [{ label: "Security parameters" }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then((data) => {
        console.log("MENU ALL ACL Set");
        this.aclMenuService.getAclMenu(this.router.url).then((dataacl) => {
          if (JSON.stringify(dataacl.acl) === "{}") {
            console.log("No ACL Founded");
          } else {
            console.log("ACL Founded");
            console.log(dataacl.acl);
            this.usractionbtn[0] = dataacl.acl.create;
            this.usractionbtn[1] = dataacl.acl.read;
            this.usractionbtn[2] = dataacl.acl.update;
            // this.usractionbtn[3] = dataacl.acl.delete;
            this.usractionbtn[4] = dataacl.acl.view;
            this.usractionbtn[5] = dataacl.acl.approval;
          }
        });
      });
    });
    this.refreshingUser();
  }
  refreshingUser() {
    this.isFetching = true;
    this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      // console.log(">>>>>>> "+JSON.stringify(data));
      if ((data.status = 200)) {
        this.sysService
          .getAllSysParam()
          .subscribe((orgall: BackendResponse) => {
            // console.log('>>>>>>> ' + JSON.stringify(orgall));
            this.userlist = orgall.data;
            if (this.userlist.length < 1) {
              let objtmp = {
                id: "No records",
                param_name: "No records",
                param_value: "No records",
                category: "No records",
                description: "No records",
              };
              this.userlist = [];
              this.userlist.push(objtmp);
            }
            this.isFetching = false;
          });
      }
    });
  }
  deleteConfirmation(data: any) {
    console.log("Di Emit nih dari child " + JSON.stringify(data));
    this.display = true;
    this.selectedUser = data;
  }
  approvalData(data: any) {
    console.log(data);
    this.viewApprove = true;
    this.selectedUser = data;
  }
  approvalSubmit(status) {
    console.log(this.selectedUser);
    console.log(status);
    let payload = {
      id: this.selectedUser.id,
      oldactive: this.selectedUser.active,
      isactive: status,
      idapproval: this.selectedUser.idapproval,
    };
    console.log(">>>>>>>> payload " + JSON.stringify(payload));
    this.sysService
      .updateSysParamActive(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>>> return "+JSON.stringify(result));
        if (result.status === 200) {
          this.refreshingUser();
          this.viewApprove = false;
        }
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: "success",
      summary: "Deleted",
      detail: message,
    });
  }
}
