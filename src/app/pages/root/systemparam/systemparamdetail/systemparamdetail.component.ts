import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MenuItem } from "primeng/api";
import { Location } from "@angular/common";
import { AuthService } from "src/app/services/auth.service";
import { BackendResponse } from "src/app/interfaces/backend-response";
import { SysparamService } from "src/app/services/servbpmp/sysparam.service";

@Component({
  selector: "app-systemparamdetail",
  templateUrl: "./systemparamdetail.component.html",
  styleUrls: ["./systemparamdetail.component.scss"],
})
export class SystemparamdetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  tokenID: string = "";
  groupForm!: FormGroup;
  submitted = false;
  orgsData: any[] = [];
  appInfoActive: any = {};
  orgSuggest: any = {};
  user: any = {};
  formatedOrg: any[] = [];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location,
    private sysService: SysparamService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf("%23") !== -1 ? true : false;
    console.log(">>>>>>>>>>> " + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { icon: "pi pi-home", routerLink: "/mgm/home" };
    this.breadcrumbs = [
      {
        label: "System Param",
        command: (event) => {
          this.location.back();
        },
        url: "",
      },
      { label: this.isEdit ? "Edit data" : "Add data" },
    ];
    this.stateOptions = [
      { label: "Direct activated", value: 1 },
      { label: "Activision link", value: 0 },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      this.groupForm = this.formBuilder.group({
        parameter: [{ value: "", disabled: true }, Validators.required],
        value: ["", Validators.required],
        description: [[], Validators.required],
      });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get("id")) {
          this.userId = this.activatedRoute.snapshot.paramMap.get("id");
          this.sysService
            .getSysParamById(this.userId)

            .subscribe(async (result: BackendResponse) => {
              console.log("Data edit user " + JSON.stringify(result.data));

              this.user.parameter = result.data.param_name;
              this.user.value = result.data.param_value;
              this.user.description = result.data.description;

              this.groupForm.patchValue({
                parameter: this.user.parameter,
                value: this.user.value,
                description: this.user.description,
              });
              console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  formatOrgData(data: any) {
    data.map((dt: any) => {
      let formated: any = {};
      formated.name = dt.groupname;
      formated.id = dt.id;
      this.formatedOrg.push(formated);
    });
  }

  filterOrg(event: any) {
    let filtered: any[] = [];
    let query = event.query;

    for (let i = 0; i < this.formatedOrg.length; i++) {
      let country = this.formatedOrg[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) > -1) {
        filtered.push(country);
      }
    }

    this.orgSuggest = filtered;
  }
  onSubmit() {
    this.submitted = true;
    console.log(">>>>>>>>IS NULL >>>> " + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      var groupacl = this.groupForm.get("orgobj")?.value;
      let payload = {};

      payload = {
        id: this.userId,
        value: this.groupForm.get("value")?.value,
        description: this.groupForm.get("description")?.value,
      };
      console.log(">>>>>>>> payload " + JSON.stringify(payload));
      this.sysService
        .updateSysParam(payload)
        .subscribe((result: BackendResponse) => {
          // console.log(">>>>>>>> return "+JSON.stringify(result));
          if (result.status === 200) {
            this.location.back();
          }
        });
    }

    console.log(this.groupForm.valid);
  }
}
