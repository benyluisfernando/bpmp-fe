import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import {
  HashLocationStrategy,
  LocationStrategy,
  PathLocationStrategy,
} from "@angular/common";
import { AppComponent } from "./app.component";
import { MessageService } from "primeng/api";
import { CardModule } from "primeng/card";
import { ToolbarModule } from "primeng/toolbar";
import { InputTextModule } from "primeng/inputtext";
import { ButtonModule } from "primeng/button";
import { SplitButtonModule } from "primeng/splitbutton";
import { MenubarModule } from "primeng/menubar";
import { PanelMenuModule } from "primeng/panelmenu";
import { DialogModule } from "primeng/dialog";
import { PanelModule } from "primeng/panel";
import { CheckboxModule } from "primeng/checkbox";
import {
  DynamicDialogModule,
  DialogService,
  DynamicDialogRef,
  DynamicDialogConfig,
} from "primeng/dynamicdialog";
import { DividerModule } from "primeng/divider";
import { MainmenulayoutComponent } from "./layout/mainmenulayout/mainmenulayout.component";
import { NomenulayoutComponent } from "./layout/nomenulayout/nomenulayout.component";
import { HomeadminComponent } from "./pages/homeadmin/homeadmin.component";
import { HomeComponent } from "./pages/home/home.component";
import { BreadcrumbModule } from "primeng/breadcrumb";
import { DropdownModule } from "primeng/dropdown";
import { BlockUIModule } from "primeng/blockui";
import { SelectButtonModule } from "primeng/selectbutton";
import { StepsModule } from "primeng/steps";
import { InputNumberModule } from "primeng/inputnumber";
import { AutoCompleteModule } from "primeng/autocomplete";
import { ChartModule } from "primeng/chart";
import { TableModule } from "primeng/table";
import { PickListModule } from "primeng/picklist";
import { TooltipModule } from "primeng/tooltip";
import { ListboxModule } from "primeng/listbox";
import { OrderListModule } from "primeng/orderlist";
import { CalendarModule } from "primeng/calendar";
import { MessagesModule } from "primeng/messages";
import { MessageModule } from "primeng/message";
import { NgxGaugeModule } from "ngx-gauge";
import { ChipModule } from "primeng/chip";
import { ProgressSpinnerModule } from "primeng/progressspinner";
import { ToastModule } from "primeng/toast";

import { ApplicationgroupComponent } from "./pages/root/applicationgroup/applicationgroup.component";
import { UsersComponent } from "./pages/root/users/users.component";
import { ApplicationsComponent } from "./pages/root/applications/applications.component";
import { SmtpaccountsComponent } from "./pages/root/smtpaccounts/smtpaccounts.component";
import { OauthsettingsComponent } from "./pages/root/oauthsettings/oauthsettings.component";
import { EventlogsComponent } from "./pages/root/eventlogs/eventlogs.component";
import { ResourceusageComponent } from "./pages/root/resourceusage/resourceusage.component";
import { ErrorpageComponent } from "./pages/errorpage/errorpage.component";
import { BackmenulayoutComponent } from "./layout/backmenulayout/backmenulayout.component";
import { LoginComponent } from "./pages/login/login.component";
import { ForgotpasswordComponent } from "./pages/forgotpassword/forgotpassword.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { InterceptorHttpService } from "./interceptors/interceptor-http.service";
import { FullmenulayoutComponent } from "./layout/fullmenulayout/fullmenulayout.component";
import { TablehelperComponent } from "./generic/tablehelper/tablehelper.component";
import { ApplicationdetailComponent } from "./pages/root/applicationgroup/applicationdetail/applicationdetail.component";
import { UserdetailComponent } from "./pages/root/users/userdetail/userdetail.component";
import { EnvServiceProvider } from "./env/env.service.provider";
import { BpmpinterceptComponent } from "./pages/bpmp/bpmphome/bpmpintercept.component";
import { SystemparamComponent } from "./pages/root/systemparam/systemparam.component";
import { SystemparamdetailComponent } from "./pages/root/systemparam/systemparamdetail/systemparamdetail.component";
import { DatePipe } from "@angular/common";
import { RecaptchaModule, RecaptchaFormsModule } from "ng-recaptcha";
import { ProfileComponent } from "./pages/root/profile/profile.component";
import { ChangePasswordComponent } from "./pages/root/change-password/change-password.component";
import { ResetpasswordComponent } from "./pages/forgotpassword/resetpassword/resetpassword.component";
import { VerifikasiemailComponent } from "./pages/forgotpassword/verifikasiemail/verifikasiemail.component";
import { BpmphomeComponent } from "./pages/bpmp/bpmphome/bpmphome.component";
import { CompanyComponent } from "./pages/bpmp/company/company.component";
import { CompanydetailComponent } from "./pages/bpmp/company/companydetail/companydetail.component";
import { ChannelComponent } from "./pages/bpmp/channel/channel.component";
import { ChanneldetailComponent } from "./pages/bpmp/channel/channeldetail/channeldetail.component";
import { CompanyreqComponent } from "./pages/bpmp/companyreq/companyreq.component";
import { ChannelreqComponent } from "./pages/bpmp/channelreq/channelreq.component";
import { RulesreqComponent } from "./pages/bpmp/rulesreq/rulesreq.component";
import { RulesreqdetailComponent } from "./pages/bpmp/rulesreq/rulesreqdetail/rulesreqdetail.component";
import { RulesComponent } from "./pages/bpmp/rules/rules.component";
import { RulesdetailComponent } from "./pages/bpmp/rules/rulesdetail/rulesdetail.component";
import { TransactionmonitoringComponent } from "./pages/bpmp/transactionmonitoring/transactionmonitoring.component";

@NgModule({
  declarations: [
    AppComponent,
    MainmenulayoutComponent,
    NomenulayoutComponent,
    HomeadminComponent,
    HomeComponent,
    ApplicationgroupComponent,
    UsersComponent,
    ApplicationsComponent,
    SmtpaccountsComponent,
    OauthsettingsComponent,
    EventlogsComponent,
    ResourceusageComponent,
    ErrorpageComponent,
    BackmenulayoutComponent,
    LoginComponent,
    ForgotpasswordComponent,
    FullmenulayoutComponent,
    TablehelperComponent,
    ApplicationdetailComponent,
    UserdetailComponent,
    BpmpinterceptComponent,
    SystemparamComponent,
    SystemparamdetailComponent,
    ProfileComponent,
    ChangePasswordComponent,
    ResetpasswordComponent,
    VerifikasiemailComponent,
    BpmphomeComponent,
    CompanyComponent,
    CompanydetailComponent,
    ChannelComponent,
    ChanneldetailComponent,
    CompanyreqComponent,
    ChannelreqComponent,
    RulesreqComponent,
    RulesreqdetailComponent,
    RulesComponent,
    RulesdetailComponent,
    TransactionmonitoringComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CardModule,
    ToolbarModule,
    ButtonModule,
    InputTextModule,
    SplitButtonModule,
    MenubarModule,
    PanelMenuModule,
    BreadcrumbModule,
    DropdownModule,
    ChartModule,
    BlockUIModule,
    DividerModule,
    ProgressSpinnerModule,
    ToastModule,
    MessagesModule,
    MessageModule,
    DialogModule,
    DynamicDialogModule,
    PanelModule,
    TableModule,
    SelectButtonModule,
    StepsModule,
    InputNumberModule,
    AutoCompleteModule,
    PickListModule,
    ListboxModule,
    OrderListModule,
    CheckboxModule,
    CalendarModule,
    ChipModule,
    NgxGaugeModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    TooltipModule,
  ],

  // import { SelectButtonModule } from 'primeng/selectbutton';
  // import { StepsModule } from 'primeng/steps';
  // import { InputNumberModule } from 'primeng/inputnumber';
  // import { AutoCompleteModule } from 'primeng/autocomplete';

  providers: [
    EnvServiceProvider,
    DynamicDialogRef,
    DynamicDialogConfig,
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorHttpService,
      multi: true,
    },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    DialogService,
    DatePipe,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
