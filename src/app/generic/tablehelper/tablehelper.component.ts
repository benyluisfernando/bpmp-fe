import {
  Component,
  OnInit,
  Input,
  ViewChild,
  Output,
  EventEmitter,
} from "@angular/core";
import { Table } from "primeng/table";

@Component({
  selector: "app-tablehelper",
  templateUrl: "./tablehelper.component.html",
  styleUrls: ["./tablehelper.component.scss"],
})
export class TablehelperComponent implements OnInit {
  isActionBtn = true;
  // records:any[] = [];
  @ViewChild("dt")
  dt!: Table;
  first = 0;
  rows = 5;

  @Input() records: any;
  @Input() wsearch: any;
  @Input() header: any;
  @Input() colnames: any;
  @Input() colwidth: any;
  @Input() colclasshalign: any;
  @Input() colmark: any;
  @Input() colldate: any;
  @Input() collink: any;
  @Input() collinkaction: any;
  @Input() actionbtn: any;
  @Input() addbtnlink: any;
  @Input() nopaging: any = true;
  @Input() scrollheight: any = "300px";
  @Output() datadeleted = new EventEmitter<any>();
  @Output() datapreview = new EventEmitter<any>();
  @Output() dataapprover = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {
    // console.log("colldate>>>>>", this.colldate);
    // console.log("Header "+JSON.stringify(this.header));
    // console.log("actbutton "+JSON.stringify(this.actionbtn));
    // console.log("addbtn "+JSON.stringify(this.addbtnlink));
    // console.log("Scroll heigh : "+JSON.stringify(this.scrollheight));
    let i = 0;
    this.actionbtn.map((dt: any) => {
      if (dt > 0) i++;
    });
    console.log(i);
    if (i < 1) {
      this.isActionBtn = false;
    }

    if (
      this.records[0].created_date === "No records" ||
      this.records[0].created_date === "N/A"
    )
      this.isActionBtn = false;
  }

  deleteConfirmation(payload: any) {
    // console.log("Di Emit nih "+JSON.stringify(payload));
    this.datadeleted.emit(payload);
  }
  previewConfirmation(payload: any) {
    this.datapreview.emit(payload);
  }
  approveConfirmation(payload: any) {
    this.dataapprover.emit(payload);
  }

  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.records ? this.first === this.records.length - this.rows : true;
  }

  isFirstPage(): boolean {
    return this.records ? this.first === 0 : true;
  }
}
