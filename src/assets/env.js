(function (window) {
  // console.log(process.env.API_URL);
  window.env = window.env || {};

  // Environment variables
  // window["env"]["apiUrl"] = "http://localhost:9005/";
  // window["env"]["baseUrl"] = "http://localhost:9004/";
  // window["env"]["apiUrl"] = "http://182.169.41.153:3000/";
  // window["env"]["baseUrl"] = "http://182.169.41.153:4013/";
  window["env"]["apiUrl"] = "http://localhost:3000/"; // krakatoa admin
  window["env"]["baseUrl"] = "http://localhost:3014/"; // bpmp
})(this);
